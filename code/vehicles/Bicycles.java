// Youry Nelson 2235372
package vehicles;

public class Bicycles{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycles(String manufact,int numGears,double maxSped){
        this.manufacturer= manufact;
        this.numberGears=numGears;
        this.maxSpeed=maxSped;
    }

    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumberGears(){
        return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public String toString(){
        return "Manufacturer: " + this.manufacturer + ", Number of gears: " + this.numberGears+ ", Max speed: " + this.maxSpeed;
    }

}