// Youry Nelson 2235372
package application;

import vehicles.Bicycles;
public class BikeStore {
    public static void main(String[] args){
    Bicycles[] bikeSold = new Bicycles[4];
    
    bikeSold[0] = new Bicycles("SuperBike", 4, 20.9);
    bikeSold[1] = new Bicycles("Bikers",3,19.5);
    bikeSold[2] = new Bicycles("BMX",5, 25);
    bikeSold[3] = new Bicycles("Unmatched",3, 26.4);

    
    for(Bicycles bike: bikeSold){
        System.out.println(bike);
    }
    }
}
